/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var phonecatApp = angular.module('phonecatApp', []);

phonecatApp.controller('PhoneListCtrl', [ '$scope', '$http',       
    function PhoneListCtrl($scope,$http){
        $http.get('phones.json').success(function(data) {
             $scope.phones = data;
        });

        $scope.orderProp = 'name';   
    }             
]);