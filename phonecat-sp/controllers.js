/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var phoneCatControllers = angular.module('phoneCatControllers', []);

phoneCatControllers.controller('PhoneListController', ['$scope', '$http',
function($scope, $http) {
	$http.get('data/phones.json').success(function(data) {
		$scope.phones = data;
	});

	$scope.orderProp = 'name';
}]);

phoneCatControllers.controller('PhoneItemController', ['$scope', '$routeParams', '$http',
function($scope, $routeParams, $http) {
	$http.get('data/' + $routeParams.phoneId + '.json').success(function(data) {
		$scope.phone = data
	});
}]); 