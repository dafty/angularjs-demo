/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var phonecatApp = angular.module('phonecatApp', [ 'ngRoute',
		'phoneCatControllers', 'phoneFilter' ]);

phonecatApp.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/phones', {
		templateUrl : 'part/list.html',
		controller : 'PhoneListController'
	}).when('/phones/:phoneId', {
		templateUrl : 'part/item.html',
		controller : 'PhoneItemController'
	}).otherwise({
		redirectTo : '/phones'
	});
} ]);
